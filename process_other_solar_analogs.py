from astroquery.simbad import Simbad

simbad = Simbad()
simbad.remove_votable_fields('coordinates')
simbad.add_votable_fields('sp')
simbad.add_votable_fields('plx')
simbad.add_votable_fields("flux(U)")
simbad.add_votable_fields("flux(B)")
simbad.add_votable_fields("flux(V)")

stars = []
counter = 0
# change type here
with open('stars_85_to_168.csv') as catalog:
    for line in catalog:
        if counter == 0:
            counter += 1
            continue
        parts = line.split(',')[0:1]

        table = simbad.query_object(parts[0])
        try:
            comps = str(table['MAIN_ID']).split()
            for i in range(len(comps)):
                if '-' in comps[i]:
                    parts[0] = ''.join(comps[i + 1:])
                    break
        except:
            continue

        U = str(table['FLUX_U']).split()[-1]
        B = str(table['FLUX_B']).split()[-1]
        V = str(table['FLUX_V']).split()[-1]
        parts.append(V)
        try:
            parts.append('%.5f'%(float(B) - float(V)))
        except:
            parts.append('-')
        try:
            parts.append('%.5f'%(float(U) - float(B)))
        except:
            parts.append('-')

        sp_type = str(table['SP_TYPE']).split()[-1]
        parts.append(sp_type)

        plx_value = 0
        d = 0
        try:
            plx_value = float(str(table['PLX_VALUE']).split()[-1]) / 1000
            d = 1/plx_value
            plx_value = '%.5f'%(plx_value)
            d = '%.5f'%(d)
        except:
            plx_value = '-'
            d = '-'
        parts.append(plx_value)
        parts.append(d)

        stars.append(parts)
        counter += 1

s = [[str(e) for e in row] for row in stars]
lens = [max(map(len, col)) for col in zip(*s)]
fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
table = [fmt.format(*row) for row in s]
with open('stars_85_to_168.txt', 'w') as out:
    out.write('\n'.join(table))