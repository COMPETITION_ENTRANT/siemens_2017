import numpy as np

sp_BSC = {}
sp_SIMBAD = {}

def getKey(item):
    return str(item[0])

with open('out.txt') as catalog:
    for line in catalog:
        parts = line.split()
        try:
            test = float(parts[4])
        except:
            continue
        if parts[6] in sp_BSC:
            sp_BSC[parts[6]].append(float(parts[4]))
        else:
            sp_BSC[parts[6]] = [float(parts[4])]
        if parts[7] in sp_SIMBAD:
            sp_SIMBAD[parts[7]].append(float(parts[4]))
        else:
            sp_SIMBAD[parts[7]] = [float(parts[4])]

combined = []
for key in sp_BSC.keys():
    sp_BSC[key] = [np.mean(sp_BSC[key]), np.std(sp_BSC[key])]
    if key not in sp_SIMBAD:
        combined.append([key] + sp_BSC[key] + ['--', '--'])
for key in sp_SIMBAD.keys():
    sp_SIMBAD[key] = [np.mean(sp_SIMBAD[key]), np.std(sp_SIMBAD[key])]
    if key in sp_BSC:
        combined.append([key] + sp_BSC[key] + sp_SIMBAD[key])
    else:
        combined.append([key] + ['--', '--'] + sp_SIMBAD[key])

combined = sorted(combined, key = getKey)

s = [[str(e) for e in row] for row in combined]
lens = [max(map(len, col)) for col in zip(*s)]
fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
table = [fmt.format(*row) for row in s]
with open('mean_std.txt', 'w') as out:
    out.write('\n'.join(table))