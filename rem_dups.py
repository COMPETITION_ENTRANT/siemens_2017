import os

directory = os.fsencode('./OtherSolarAnalogs(txt)')

stars = []
rep = []
for file in os.listdir(directory):
    filename = os.fsdecode(file)
    if filename.endswith(".txt"): 
        counter = 0
        with open("./OtherSolarAnalogs(txt)/" + filename) as catalog:
            for line in catalog:
                if counter < 5:
                    counter += 1
                    continue
                
                parts = line.split()
                if parts[0] in rep:
                    continue
                else:
                    stars.append(parts)
                    rep.append(parts[0])

                counter += 1

s = [[str(e) for e in row] for row in stars]
lens = [max(map(len, col)) for col in zip(*s)]
fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
table = [fmt.format(*row) for row in s]
with open('final.txt', 'w') as out:
    out.write('\n'.join(table))
