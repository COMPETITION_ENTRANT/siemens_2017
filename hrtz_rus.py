import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.patches as patches

import warnings
warnings.filterwarnings('error')

# absolute visual magnitude of the Sun = 4.862
# B-V magnitude of the Sun = 0.653


min_x = 0.5
max_x = 1.0
min_y = 3.0
max_y = 7.0


mpl.rcParams['font.family']='serif'
mpl.rcParams['font.size']=14

# plt.figure().suptitle('$Hertzsprung–Russell\ Diagram$', fontsize = 18)
plt.xlabel('$B-V$')
plt.ylabel('$M_V\ (mag)$')

V = []
B_V = []
# sp_types = []

# V_open = []
# B_V_open = []

with open('final.csv') as catalog:
    for line in catalog:
        parts = line.split(',')
        try:
            # sp_type = parts[4]
            # if sp_type[0] == 'G' and int(sp_type[1]) >= 0 and int(sp_type[1]) <= 4:
            d = float(parts[6])
            V.append(float(parts[1]) - 5 * np.log10(d) + 5)
            B_V.append(float(parts[2]))
            # sp_types.append(parts[6])
            # else:
                # d = float(parts[6])
                # V_open.append(float(parts[1]) - 5 * np.log10(d) + 5)
                # B_V_open.append(float(parts[2]))
        except:
            continue

'''V = [v for (sp, v) in sorted(zip(sp_types, V))]
B_V = [b_v for (sp, b_v) in sorted(zip(sp_types, B_V))]
sp_types = sorted(sp_types)

colors = [0 for i in range(0, len(sp_types))]
keys = [sp_types[0]]
counter = 0
for i in range(1, len(sp_types)):
    if sp_types[i] == sp_types[i - 1]:
        colors[i] = colors[i - 1]
    else:
        counter += 1
        colors[i] = counter
        keys.append(sp_types[i])
print(keys)'''

# FILTER!
# i = 0
# while i < len(B_V):
#     if B_V[i] < min_x:
#         del B_V[i]
#         del V[i]
#     elif B_V[i] > max_x:
#         del B_V[i]
#         del V[i]
#     elif V[i] < min_y:
#         del B_V[i]
#         del V[i]
#     elif V[i] > max_y:
#         del B_V[i]
#         del V[i]
#     else:
#         i += 1
# i = 0
# while i < len(B_V_open):
#     if B_V_open[i] < min_x:
#         del B_V_open[i]
#         del V_open[i]
#     elif B_V_open[i] > max_x:
#         del B_V_open[i]
#         del V_open[i]
#     elif V_open[i] < min_y:
#         del B_V_open[i]
#         del V_open[i]
#     elif V_open[i] > max_y:
#         del B_V_open[i]
#         del V_open[i]
#     else:
#         i += 1

s = [10 for i in range(len(V))]
# s_open = [25 for i in range(len(V_open))]
plt.scatter(B_V, V, c = 'black', edgecolors = 'none', s = s) # , c = colors, cmap = 'RdBu_r')
plt.gca().add_patch(patches.Rectangle((0.553, 3.862), 0.2, 2, fill = False, edgecolor = 'orange'))
# plt.scatter(B_V_open, V_open, facecolors='none', edgecolors='b', s = s_open) # , c = colors, cmap = 'RdBu_r')
plt.scatter(0.653, 4.83, c = 'orange', edgecolors = 'none') # , c = colors, cmap = 'RdBu_r')
plt.gca().invert_yaxis()
'''cb = plt.colorbar(cc)
cb.set_ticks(range(len(keys)))
cb.set_ticklabels(keys)'''
plt.show()