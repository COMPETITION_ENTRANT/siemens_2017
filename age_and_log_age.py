from astroquery.simbad import Simbad
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib as mpl
from pylab import *
from sklearn.metrics import mean_squared_error

import scipy.optimize as sp

import warnings
warnings.filterwarnings('error')

catalog = 0
coeffs = {}
underscore = ''
power = 1

def reject_outliers(arr_x, arr_y):
    x_q1, x_q3 = np.percentile(arr_x, [25, 75])
    y_q1, y_q3 = np.percentile(arr_y, [25, 75])
    x = []
    y = []
    for i in range(0, len(arr_x)):
        if arr_x[i] < x_q1 - 1.5 * (x_q3 - x_q1) or arr_x[i] > x_q3 + 1.5 * (x_q3 - x_q1) or arr_y[i] < y_q1 - 1.5 * (y_q3 - y_q1) or arr_y[i] > y_q3 + 1.5 * (y_q3 - y_q1):
            continue
        if arr_x[i] < -2.5 or arr_x[i] > -0.5:
            continue
        if arr_y[i] == 0 or arr_y[i] > 13.772:
            continue
        x.append(arr_x[i])
        y.append(arr_y[i])
    return (x, y)

def run(mini, maxi):
    global catalog
    global coeffs
    global underscore
    global power

    simbad = Simbad()
    simbad.remove_votable_fields('coordinates')
    simbad.add_votable_fields('plx')
    simbad.add_votable_fields("flux(B)")
    simbad.add_votable_fields("flux(V)")

    id_to_age = {}

    with open('ages_master.csv') as f:
        for line in f:
            parts = line.strip().split(',')
            try:
                if catalog == 25 or catalog == 38:
                    id_to_age[parts[0]] = 10.0 ** (float(parts[catalog].strip().split()[0]) - 9.0)
                elif catalog == 33:
                    id_to_age[parts[0]] = float(parts[catalog].strip().split()[0])
                elif catalog == 65:
                    id_to_age[parts[0]] = float(parts[catalog].split(':')[1].strip().split()[0]) / 10.0 ** 3
                else:
                    id_to_age[parts[0]] = float(parts[catalog].split(':')[1].strip().split()[0])
            except:
                continue

    id_to_FUV = {}

    with open('table.csv') as f:
        for line in f:
            parts = line.strip().split(',')
            id_to_FUV[parts[0]] = float(parts[5])

    num_to_ref = {}

    with open('key.csv') as f:
        for line in f:
            parts = line.strip().split(',')
            num_to_ref[int(parts[0])] = parts[1]

    B_V = []
    Vmag = []
    FUV_B = []
    age_q = []

    for star in id_to_age.keys():
        table = simbad.query_object(star)
        B = 0
        V = 0
        try:
            B = float(str(table['FLUX_B']).split()[-1])
        except:
            continue
        try:
            V = float(str(table['FLUX_V']).split()[-1])
        except:
            continue

        if B - V < mini or B - V > maxi:
            continue

        plx_value = 0
        d = 0
        try:
            plx_value = float(str(table['PLX_VALUE']).split()[-1]) / 1000
            d = 1 / plx_value
        except:
            continue

        try:
            FUV_B.append(id_to_FUV[star] - B)
        except:
            continue
        age_q.append(id_to_age[star])
        B_V.append(B - V)
        Vmag.append(V - 5 * np.log10(d) + 5)

    Q_FUV_B = [-(-29.845 * B_V[i] ** 2 + 48.879 * B_V[i] - 6.5257 - FUV_B[i]) for i in range(0, len(B_V))]
    if len(Q_FUV_B) == 0:
        return -100

    x_q, y_q = reject_outliers(Q_FUV_B, age_q)
    print(len(x_q))
    print(sum(B_V) / len(B_V))

    mpl.rcParams['font.family']='serif'
    mpl.rcParams['font.size']=14

    f, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
    s_q = [10 for i in range(len(x_q))]
    
    if (str(catalog) + underscore) in coeffs:
        p_x = np.linspace(np.min(x_q), np.max(x_q), 100)

        p_y_1 = np.add(np.multiply(coeffs[(str(catalog) + underscore)][0], p_x), coeffs[(str(catalog) + underscore)][1])
        p_y_2 = np.exp(np.multiply(coeffs[(str(catalog) + underscore)][0], p_x)) * np.exp(coeffs[(str(catalog) + underscore)][1])

    ax1.scatter(x_q, np.log(y_q), c = 'k', s = s_q)
    if (str(catalog) + underscore) in coeffs:
        ax1.plot(p_x, p_y_1)
    ax2.scatter(x_q, y_q, c = 'k', s = s_q)
    if (str(catalog) + underscore) in coeffs:
        ax2.plot(p_x, p_y_2)
    
    f.text(0.5, 0.01, '$Q$', ha='center')
    f.text(0.01, 0.75, "$log_e\ Age\ (log_e\ Gyr)$", va='center', rotation='vertical')
    f.text(0.01, 0.25, '$Age\ (Gyr)$', va='center', rotation='vertical')

    slope, intercept, r_q, p_q, std_err = stats.linregress(x_q, np.log(y_q))
    q_s_1 = '$log-linear\ r^2\ =\ $' + str("{0:.3f}".format(round(r_q ** 2, 3)))
    q_s_2 = '\n' + '$log-linear\ p-value\ =\ $' + str("{0:.3f}".format(round(p_q, 3)))
    ax1.set_title(q_s_1 + q_s_2, fontdict = {'fontsize': 14})

    # plt.suptitle(num_to_ref[catalog] + '\n' + 'B-V: [' + str("{0:.3f}".format(mini)) + ', ' + str("{0:.3f}".format(maxi)) + ']', fontsize = 18)

    if (str(catalog) + underscore) in coeffs:
        plt.savefig(str(catalog) + '_' + str(mini) + '_' + str(maxi) + '.png')
    plt.close()

    return (x_q, y_q)

with open('opt_3.txt') as f:
    for line in f:
        parts = line.strip().split()
        catalog = int(parts[0])
        print(catalog)

        '''for b_v in np.arange(0.553, 0.763, 0.04):
            x, y = run(b_v, b_v + 0.04)
            try:
                p = return_fit(x[:], np.log(y[:]).tolist(), power, np.sqrt(y[:]).tolist())
            except:
                p = np.polyfit(x, np.log(y), power, w = np.sqrt(y))
            print('B-V', b_v, 'to', b_v + 0.04, 'a:', np.exp(p[1]), 'b:', p[0])'''

        x, y = run(0.553, float(parts[1]))
        p = np.polyfit(x, np.log(y), power) #, w = np.sqrt(y))
        print('B-V', 0.553, 'to', parts[1], 'a:', np.exp(p[1]), 'b:', p[0])
        coeffs[str(catalog) + '_1'] = p

        x, y = run(float(parts[1]), float(parts[2]))
        p = np.polyfit(x, np.log(y), power) #, w = np.sqrt(y))
        print('B-V', parts[1], 'to', parts[2], 'a:', np.exp(p[1]), 'b:', p[0])
        coeffs[str(catalog) + '_2'] = p

        x, y = run(float(parts[2]), 0.753)
        p = np.polyfit(x, np.log(y), power) #, w = np.sqrt(y))
        print('B-V', parts[2], 'to', 0.753, 'a:', np.exp(p[1]), 'b:', p[0])
        coeffs[str(catalog) + '_3'] = p

        print()
        print()

with open('opt_3.txt') as f:
    for line in f:
        parts = line.strip().split()

        catalog = int(parts[0])
        underscore = '_1'
        run(0.553, float(parts[1]))
        underscore = '_2'
        run(float(parts[1]), float(parts[2]))
        underscore = '_3'
        run(float(parts[2]), 0.753)
