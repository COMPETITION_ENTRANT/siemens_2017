vizier = {}
with open("150_vizier_data.tsv") as catalog:
    counter = 0
    for line in catalog:
        if counter < 25:
            counter += 1
            continue

        parts = line.split()
        if len(parts) == 7:
            b = int(parts[5])
            if b == 1:
                parts.insert(2, "--")
                parts.insert(3, "--")
            else:
                parts.insert(4, "--")
                parts.insert(5, "--")
        elif len(parts) != 7 and len(parts) != 9:
            continue

        vizier[parts[0]] = parts[2:]
        counter += 1

stars = []
with open("out.txt") as catalog:
    for line in catalog:
        parts = line.split()

        hd_num = "HD" + parts[1]

        if hd_num not in vizier:
            parts.extend(["--" for i in range(7)])
        else:
            parts.extend(vizier[hd_num])

        stars.append(parts)

s = [['HR', 'HD', 'ADS# SIMBAD M&M', 'Vmag (mag) M&M', 'B-V (mag) M&M', 'U-B (mag) M&M', 'SpType BSC', 'SpType SIMBAD', 'Parallax SIMBAD', 'Distance d (1/parallax)', 'FUV mag', 'e_FUV mag', 'NUV mag', 'e_NUV mag', 'r.fov deg', 'b mag', 'E(B-V)']] + [[str(e) for e in row] for row in stars]
lens = [max(map(len, col)) for col in zip(*s)]
fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
table = [fmt.format(*row) for row in s]
with open('out_2.tsv', 'w') as out:
    out.write('\n'.join(table))
