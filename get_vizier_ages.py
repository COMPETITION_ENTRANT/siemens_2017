from astroquery.vizier import Vizier
from astropy import units as u

q = u.Quantity(0.2, u.arcmin)

v = Vizier(keywords=['ages'])

stars = []
with open('final.csv') as f:
    for line in f:
        stars.append(line.strip().split(',')[0])

out = {}
for star in stars:
    result = v.query_object(star, radius = q)
    out[star] = {}

    for catalog in result.keys():
        print(catalog)
        table = result[catalog]

        keep = []
        for colname in table.colnames:
            unit_type = 0
            try:
                unit_type = table[colname].unit._names[0]
            except:
                continue

            if 'yr' in unit_type.lower():
                keep.append([colname, unit_type])

        out[star][catalog] = []
        for pair in keep:
            colname = pair[0]
            unit_type = pair[1]
            val = str(table[colname]).split('\n')
            out[star][catalog].append(val[0] + ': ' + val[-1] + ' ' + unit_type)

master_list = {}
counter = 44
for star in out.keys():
    for catalog in out[star].keys():
        if catalog not in master_list:
            master_list[catalog] = counter
            counter += 1

'''table = [','.join(['Star'] + [str(x) for x in range(1, len(master_list.keys()) + 1)])]
for star in out.keys():
    temp = [star] + [str(0) for x in range(0, len(master_list.keys()))]
    for catalog in out[star].keys():
        index = master_list[catalog]
        temp[index + 1] = '|'.join(out[star][catalog])
    table.append(','.join(temp))

with open('vizier_ages_data.csv', 'w') as f:
    for row in table:
        f.write(row + '\n')
    for catalog in master_list.keys():
        f.write(str(master_list[catalog]) + ',' + catalog + '\n')'''

with open('vizier_ages.csv', 'w') as f:
    for star in out.keys():
        for catalog in out[star].keys():
            f.write(star + ',' + '|'.join(out[star][catalog]) + ',' + str(master_list[catalog]) + '\n')
    for catalog in master_list.keys():
        f.write(str(master_list[catalog]) + ',' + catalog + '\n')


