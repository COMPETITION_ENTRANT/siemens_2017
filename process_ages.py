from astroquery.simbad import Simbad

simbad = Simbad()
simbad.remove_votable_fields('coordinates')

num_papers = 116

stars = {}
with open('solar_analog_ages.csv') as catalog:
    for line in catalog:
        parts = line.split(',')

        try:
            table = simbad.query_object(parts[0])
            comps = str(table['MAIN_ID']).split()
            for i in range(len(comps)):
                if '-' in comps[i]:
                    parts[0] = ''.join(comps[i + 1:])
                    break
        except:
            continue

        if parts[0] not in stars:
            stars[parts[0]] = ['--' for i in range(num_papers)]
        stars[parts[0]][int(parts[2]) - 1] = parts[1]

s = [[str(i) for i in range(num_papers + 1)]] + [[key] + [str(e) for e in stars[key]] for key in stars.keys()]
lens = [max(map(len, col)) for col in zip(*s)]
fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
table = [fmt.format(*row) for row in s]
with open('ages.tsv', 'w') as out:
    out.write('\n'.join(table))
