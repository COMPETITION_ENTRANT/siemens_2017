import matplotlib.pyplot as plt
import matplotlib as mpl

import numpy as np

import warnings
warnings.filterwarnings('error')

mpl.rcParams['font.family']='serif'
mpl.rcParams['font.size']=14

plt.figure().suptitle('$FUV-B\ vs.\ B-V$', fontsize = 18)
plt.ylabel('$FUV-B$')
plt.xlabel('$B-V$')

galex_6_7 = {}

with open('our_stars_galex67.csv') as f:
    for line in f:
        parts = line.split(',')

        if parts[0] in galex_6_7 or int(parts[6]) == 1:
            continue

        galex_6_7[parts[0]] = [float(parts[8])]

to_del = []
with open('our_stars.csv') as f:
    for line in f:
        parts = line.split(',')

        if parts[0] not in galex_6_7:
            continue

        try:
            if float(parts[2]) >= 0.50 and float(parts[2]) <= 0.90:
                galex_6_7[parts[0]].append(float(parts[2]))
                galex_6_7[parts[0]].append(float(parts[1]))
            else:
                err = int('-')
        except:
            to_del.append(parts[0])

for star in to_del:
    del galex_6_7[star]

FUV_B = []
B_V = []

for star in galex_6_7.keys():
    FUV = galex_6_7[star][0]
    BminV = galex_6_7[star][1]
    V = galex_6_7[star][2]
    FUV_B.append(FUV - (BminV + V))
    B_V.append(BminV)

curve_x = [x for x in np.arange(0.5, 0.92, 0.02)]
curve_y = [-29.845 * x ** 2 + 48.879 * x - 6.5257 for x in curve_x]

s = [25 for i in range(len(FUV_B))]
plt.scatter(B_V, FUV_B, c = 'k', edgecolors = 'none', s = s)
plt.plot(curve_x, curve_y, c = 'b')
plt.show()