stars = []
with open('final.csv') as f:
    for line in f:
        parts = line.strip().split(',')
        try:
            if float(parts[2]) > 0.57 and float(parts[2]) <= 0.69:
                stars.append([parts[0], parts[2]])
        except:
            continue

with open('our_stars_names.txt', 'w') as f:
    for star in stars:
        f.write(star[0] + ',' + star[1] + '\n')
