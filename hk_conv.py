from astroquery.simbad import Simbad

simbad = Simbad()
simbad.remove_votable_fields('coordinates')

stars = []
with open('cal_HK.csv') as f:
    for line in f:
        parts = line.strip().split(',')
        table = simbad.query_object(parts[0])
        try:
            comps = str(table['MAIN_ID']).split()
            for i in range(len(comps)):
                if '-' in comps[i]:
                    parts[0] = ''.join(comps[i + 1:])
                    print(parts[0])
                    break
        except:
            continue
        stars.append(parts)

with open('out.csv', 'w') as f:
    for row in stars:
        f.write(','.join(row) + '\n')
