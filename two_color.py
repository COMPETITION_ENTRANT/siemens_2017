import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl



min_x = 0.5
max_x = 1.0
min_y = 0.0
max_y = 1.0



mpl.rcParams['font.family']='serif'
mpl.rcParams['font.size']=14

plt.figure().suptitle('$2-Color\ Diagram$', fontsize = 18)
plt.xlabel('$B-V\ (mag)$')
plt.ylabel('$U-B\ (mag)$')

U_B = []
B_V = []
# sp_types = []

with open('final.csv') as catalog:
    for line in catalog:
        parts = line.split(',')
        try:
            U_B.append(float(parts[3]))
            B_V.append(float(parts[2]))
            # sp_types.append(parts[6])
        except:
            continue

'''V = [v for (sp, v) in sorted(zip(sp_types, V))]
B_V = [b_v for (sp, b_v) in sorted(zip(sp_types, B_V))]
sp_types = sorted(sp_types)

colors = [0 for i in range(0, len(sp_types))]
keys = [sp_types[0]]
counter = 0
for i in range(1, len(sp_types)):
    if sp_types[i] == sp_types[i - 1]:
        colors[i] = colors[i - 1]
    else:
        counter += 1
        colors[i] = counter
        keys.append(sp_types[i])
print(keys)'''

# FILTER!
i = 0
while i < len(B_V):
    if B_V[i] < min_x:
        del B_V[i]
        del U_B[i]
    elif B_V[i] > max_x:
        del B_V[i]
        del U_B[i]
    elif U_B[i] < min_y:
        del B_V[i]
        del U_B[i]
    elif U_B[i] > max_y:
        del B_V[i]
        del U_B[i]
    else:
        i += 1

cc = plt.scatter(B_V, U_B, c = 'k', edgecolors = 'none') # , c = colors, cmap = 'RdBu_r')
plt.gca().invert_yaxis()
'''cb = plt.colorbar(cc)
cb.set_ticks(range(len(keys)))
cb.set_ticklabels(keys)'''
plt.show()