from astroquery.simbad import Simbad
import numpy as np
from scipy import stats
import matplotlib.pyplot as plt
import matplotlib as mpl
from pylab import *

def run(catalog, mini, maxi):
    simbad = Simbad()
    simbad.remove_votable_fields('coordinates')
    simbad.add_votable_fields('plx')
    simbad.add_votable_fields("flux(B)")
    simbad.add_votable_fields("flux(V)")

    id_to_age = {}

    with open('ages_master.csv') as f:
        for line in f:
            parts = line.strip().split(',')
            try:
                id_to_age[parts[0]] = float(parts[catalog].split(':')[1].strip().split()[0])
            except:
                continue

    id_to_FUV = {}

    with open('table.csv') as f:
        for line in f:
            parts = line.strip().split(',')
            id_to_FUV[parts[0]] = float(parts[5])

    num_to_ref = {}

    with open('key.csv') as f:
        for line in f:
            parts = line.strip().split(',')
            num_to_ref[int(parts[0])] = parts[1]

    B_V_2 = []
    B_V = []
    B_V_10 = []

    Vmag_2 = []
    Vmag = []
    Vmag_10 = []

    FUV_B_2 = []
    FUV_B = []
    FUV_B_10 = []

    age_2 = []
    age = []
    age_10 = []

    for star in id_to_age.keys():
        table = simbad.query_object(star)
        B = 0
        V = 0
        try:
            B = float(str(table['FLUX_B']).split()[-1])
        except:
            continue
        try:
            V = float(str(table['FLUX_V']).split()[-1])
        except:
            continue

        if B - V < mini or B - V > maxi:
            continue

        plx_value = 0
        d = 0
        try:
            plx_value = float(str(table['PLX_VALUE']).split()[-1]) / 1000
            d = 1 / plx_value
        except:
            continue

        if id_to_age[star] <= 2:
            try:
                FUV_B_2.append(id_to_FUV[star] - B)
            except:
                continue
            age_2.append(id_to_age[star])
            B_V_2.append(B - V)
            Vmag_2.append(V - 5 * np.log10(d) + 5)
        elif id_to_age[star] >= 10:
            try:
                FUV_B_10.append(id_to_FUV[star] - B)
            except:
                continue
            age_10.append(id_to_age[star])
            B_V_10.append(B - V)
            Vmag_10.append(V - 5 * np.log10(d) + 5)
        else:
            try:
                FUV_B.append(id_to_FUV[star] - B)
            except:
                continue
            age.append(id_to_age[star])
            B_V.append(B - V)
            Vmag.append(V - 5 * np.log10(d) + 5)

    curve_x = [x for x in np.arange(0.0, 1.3, 0.02)]
    curve_y = [-29.845 * x ** 2 + 48.879 * x - 6.5257 for x in curve_x]

    mpl.rcParams['font.family']='serif'
    mpl.rcParams['font.size']=14

    f, axarr = plt.subplots(3, sharex=True)
    s = [10 for i in range(len(B_V))]

    axarr[0].scatter((B_V_2 + B_V + B_V_10), (age_2 + age + age_10), c = 'k', s = s)

    axarr[1].scatter(B_V_2, FUV_B_2, c = 'b', s = s)
    axarr[1].scatter(B_V, FUV_B, c = 'k', s = s)
    axarr[1].plot(curve_x, curve_y, c = 'k')
    axarr[1].scatter(B_V_10, FUV_B_10, c = 'r', s = s)

    axarr[2].scatter(B_V_2, Vmag_2, c = 'b', s = s)
    axarr[2].scatter(B_V, Vmag, c = 'k', s = s)
    axarr[2].scatter(B_V_10, Vmag_10, c = 'r', s = s)
    axarr[2].scatter(0.653, 4.83, c = 'orange', edgecolors = 'none')
    axarr[2].invert_yaxis()

    f.text(0.5, 0.01, '$B-V\ (mag)$', ha='center')
    f.text(0.01, 0.17, '$M_V\ (mag)$', va='center', rotation='vertical')
    f.text(0.01, 0.5, '$FUV-B\ (mag)$', va='center', rotation='vertical')
    f.text(0.01, 0.83, '$Age\ (Gyr)$', va='center', rotation='vertical')

    fig = gcf()
    fig.suptitle('$' + num_to_ref[catalog] + '$', fontsize = 18)

    plt.show()

    FUV_B = FUV_B_2 + FUV_B + FUV_B_10
    B_V = B_V_2 + B_V + B_V_10
    Q_FUV_B = [-(-29.845 * B_V[i] ** 2 + 48.879 * B_V[i] - 6.5257 - FUV_B[i]) for i in range(0, len(B_V))]
    age = age_2 + age + age_10

    if len(Q_FUV_B) == 0:
        return

    x_q1, x_q3 = np.percentile(Q_FUV_B, [25, 75])
    y_q1, y_q3 = np.percentile(age, [25, 75])
    x = []
    y = []
    for i in range(0, len(Q_FUV_B)):
        if Q_FUV_B[i] < x_q1 - 1.5 * (x_q3 - x_q1) or Q_FUV_B[i] > x_q3 + 1.5 * (x_q3 - x_q1) or age[i] < y_q1 - 1.5 * (y_q3 - y_q1) or age[i] > y_q3 + 1.5 * (y_q3 - y_q1):
            continue
        x.append(Q_FUV_B[i])
        y.append(age[i])

    if len(x) == 0:
        return

    s = [10 for i in range(len(FUV_B))]
    plt.figure().suptitle('$Age\ vs.\ Q(FUV-B)\ (' + num_to_ref[catalog] + ')$', fontsize = 18)
    plt.xlabel('$Q(FUV-B)$')
    plt.ylabel('$Age\ (Gyr)$')
    plt.scatter(x, y, c = 'k', s = s)
    plt.show()
    # plt.savefig(str(catalog) + '_' + str(mini) + '_' + str(maxi) + '.png')
    # plt.close()

    slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
    print('Catalog:', catalog, 'Mini:', mini, 'Maxi:', maxi, 'Rsq:', r_value ** 2, 'Number of stars:', len(x))

with open('opt_3.txt') as f:
    for line in f:
        parts = line.strip().split()

        catalog = int(parts[0])
        run(catalog, 0, 10)
