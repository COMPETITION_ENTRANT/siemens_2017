import matplotlib.pyplot as plt
import matplotlib as mpl

import warnings
warnings.filterwarnings('error')

mpl.rcParams['font.family']='serif'
mpl.rcParams['font.size']=14

# plt.figure().suptitle('$GALEX\ Versions\ Star\ Count\ Comparison$', fontsize = 18)
plt.xlabel('$GALEX\ 3/4$')
plt.ylabel('$GALEX\ 6/7$')

galex_3_4 = {}

with open('3_4.csv') as f:
    for line in f:
        parts = line.split(',')

        fuv_mag = 0
        try:
            fuv_mag = float(parts[10])
        except:
            continue

        galex_3_4['HD' + parts[1]] = fuv_mag

galex_6_7 = {}

with open('6_7.csv') as f:
    for line in f:
        parts = line.split(',')

        if parts[0] in galex_6_7 or int(parts[6]) == 1:
            continue

        galex_6_7[parts[0]] = float(parts[8])

g_3_4 = []
g_6_7 = []

not_in_6_7 = []

for x in galex_3_4.keys():
    if x in galex_6_7:
        g_3_4.append(galex_3_4[x])
        g_6_7.append(galex_6_7[x])
    else:
        not_in_6_7.append(x)

not_in_3_4 = []

for x in galex_6_7.keys():
    if x not in galex_3_4:
        not_in_3_4.append(x)

print()
print()
print(len(not_in_3_4))
print(not_in_3_4)
print()
print()
print(len(not_in_6_7))
print(not_in_6_7)
print()
print()


s = [25 for i in range(len(g_3_4))]
plt.scatter(g_3_4, g_6_7, c = 'k', edgecolors = 'none', s = s)
plt.show()



