stars = []

with open("our_stars_limited.tsv") as f:
    for line in f:
        parts = line.split()
        stars.append(','.join(parts))

with open("our_stars_limited.csv", 'w') as f:
    for star in stars:
        f.write(star + "\n")

