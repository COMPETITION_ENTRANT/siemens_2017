import numpy as np

import warnings
warnings.filterwarnings('error')

stars = {}
order = []

with open('table.csv') as f:
    for line in f:
        parts = line.split(',')
        if parts[0] in stars:
            stars[parts[0]][0].append(parts[1])
            stars[parts[0]][1].append(parts[3])
        else:
            stars[parts[0]] = [[parts[1]], [parts[3]]]
        parts[-1] = parts[-1].strip()
        order.append(parts)

to_delete = []
for star in stars.keys():
    FUV = stars[star][0]
    NUV = stars[star][1]

    FUV = [float(x) for x in FUV if float(x) != -999]
    NUV = [float(x) for x in NUV if float(x) != -999]

    if len(FUV) == 0:
        to_delete.append(star)

    try:
        mean_fuv = np.mean(FUV)
    except:
        mean_fuv = '-'
    try:
        mean_nuv = np.mean(NUV)
    except:
        mean_nuv = '-'

    try:
        range_fuv = np.ptp(FUV)
    except:
        range_fuv = '-'
    try:
        range_nuv = np.ptp(NUV)
    except:
        range_nuv = '-'

    stars[star] = [mean_fuv, range_fuv, mean_nuv, range_nuv]

for star in to_delete:
    del stars[star]
    order = [parts for parts in order if parts[0] != star]

s = []
for key in order:
    s.append(key + [str(e) for e in stars[key[0]]])
lens = [max(map(len, col)) for col in zip(*s)]
fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
table = [fmt.format(*row) for row in s]
with open('table.tsv', 'w') as out:
    out.write('\n'.join(table))


