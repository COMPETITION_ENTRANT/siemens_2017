from astroquery.simbad import Simbad

simbad = Simbad()
simbad.remove_votable_fields('coordinates')
simbad.add_votable_fields('sp')
simbad.add_votable_fields('plx')
simbad.add_votable_fields("flux(U)")
simbad.add_votable_fields("flux(B)")
simbad.add_votable_fields("flux(V)")

stars = []
with open('bsc_abr.tsv') as catalog:
    for line in catalog:
        parts = line.split()

        table = simbad.query_object("HD" + parts[1])

        U = str(table['FLUX_U']).split()[-1]
        B = str(table['FLUX_B']).split()[-1]
        V = str(table['FLUX_V']).split()[-1]
        if parts[3].strip() == '-':
            parts[3] = U
        if parts[4].strip() == '-':
            try:
                parts[4] = float(B) - float(V)
            except:
                parts[4] = '--'
        if parts[5].strip() == '-':
            try:
                parts[5] = float(U) - float(B)
            except:
                parts[5] = '--'

        sp_type = str(table['SP_TYPE']).split()[-1]
        parts[7] = sp_type

        if 'III' in sp_type or '+' in sp_type:
            continue

        plx_value = 0
        d = 0
        try:
            plx_value = float(str(table['PLX_VALUE']).split()[-1]) / 1000
            d = 1/plx_value
            plx_value = '%.5f'%(plx_value)
            d = '%.5f'%(d)
        except:
            plx_value = '--'
            d = '--'
        parts[8] = plx_value
        parts[9] = d

        stars.append(parts)

s = [[str(e) for e in row] for row in stars]
lens = [max(map(len, col)) for col in zip(*s)]
fmt = '\t'.join('{{:{}}}'.format(x) for x in lens)
table = [fmt.format(*row) for row in s]
with open('out.txt', 'w') as out:
    out.write('\n'.join(table))