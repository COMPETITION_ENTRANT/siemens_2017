import matplotlib.pyplot as plt
import matplotlib as mpl

import warnings
warnings.filterwarnings('error')

mpl.rcParams['font.family']='serif'
mpl.rcParams['font.size']=14

B_V_min = 0 # 0.57
B_V_max = 100 # 0.61
V_min = 0 #4.36
V_max = 100 #5.36

plt.figure().suptitle('$FUV-B\ vs.\ Age\ (' + str(B_V_min) + ',' + str(B_V_max) + ']$', fontsize = 18)
plt.xlabel('$FUV-B$')
plt.ylabel('$Age$')

V_B_V = {}

with open('final.csv') as f:
    for line in f:
        parts = line.split(',')
        try:
            if float(parts[2]) > B_V_min and float(parts[2]) <= B_V_max:
                if float(parts[1]) >= V_min and float(parts[1]) <= V_max:
                    V_B_V[parts[0]] = [float(parts[2]), float(parts[1])]
            else:
                err = int('-')
        except:
            continue

FUV = {}
with open('our_stars_galex67.csv') as f:
    for line in f:
        parts = line.split(',')
        try:
            FUV[parts[0]] = float(parts[8])
        except:
            continue

FUV_B = []
ages = []
with open('catalog_25.csv') as f:
    for line in f:
        if line.strip().endswith(','):
            continue
        parts = line.split(',')
        ages.append(float(parts[1]))
        try:
            FUV_B.append(FUV[parts[0]] - sum(V_B_V[parts[0]]))
        except:
            del ages[-1]
            continue

s = [25 for i in range(len(FUV_B))]
plt.scatter(FUV_B, ages, c = 'k', edgecolors = 'none', s = s)
plt.show()